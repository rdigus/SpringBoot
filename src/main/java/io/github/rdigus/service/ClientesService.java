package io.github.rdigus.service;

import io.github.rdigus.model.Cliente;
import io.github.rdigus.repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientesService {

    private ClientesRepository repository;

    public ClientesService(ClientesRepository repository) {
        this.repository = repository;
    }

    public void salvarClinte(Cliente cliente) {
        validarClinte(cliente);
        this.repository.persist(cliente);
    }

    public void validarClinte(Cliente cliente) {
    //aplicar as validações
    }
}
